---
title: "Working with Editors"
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - git
        - editors
        - atom
page-toc:
    active: true
---

# Editors
The easiest way to work and edit the **Howto** files is through a text editor with Git integrated.

As we mentioned before, for practical reasons, we will only see how to do it in **Atom Text Editor**, although you can choose to use any other. Along with every step to follow in the editor we will also see the Git commands in the terminal to learn and understand what are Atom and Git doing.

We will try to add more howtos about using other editors in the future. If you want and have time, you can also write one yourself about your favorite editor and we can add it to this section.


## [Atom](atom/interface)
