---
title: Contribuer au How-to: Forum
published: false
visible: true
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - forum
page-toc:
    active: false
---


# Tâche ouverte

Ceci est un bon exemple du travail qu'il reste à faire. Jusqu'à présent, personne n'a été trouvé pour écrire ce tutoriel. Peut-être voulez-vous en faire votre premier projet ?
