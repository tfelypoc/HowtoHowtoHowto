---
title: How-to: Contribute
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
page-toc:
    active: false
---

# Contribute

We think that knowledge is a collective construction. In other words, knowledge is the result of working together and cooperatively, as a community.

Disroot platform cost money besides our most precious value: time. While the costs of maintaining all the services working on started being covered a few months ago thanks to donations, documentation requires time.
*"A lot of precious time..."*

For those users who may want to contribute to Disroot by donating their time and knowhow, we've tried to channel all the efforts through this section.
Here you'll find basic information and guidelines for different ways to contribute, from feedback to write a how-to or translate them to your language.

Thanks to all of you who support and collaborate with Disroot.


![](contribute.png)
