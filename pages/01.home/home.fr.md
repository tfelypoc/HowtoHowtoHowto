---
title: Home
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Bienvenue sur le Site de Tutoriels de Disroot.

Le but principal de ce site est de vous aider à naviguer au travers des divers services **Disroot**.

Parcourir tous les services, ainsi que toutes les fonctionnalités offertes par **Disroot**, pour toutes les plateformes et/ou systèmes d'exploitation est un projet vaste, ambitieux et très chronophage, qui requiert une très grande quantité de travail. Nous pensons qu'il pourrait être utile, non seulement pour les utilisateurs (disrooters) mais pour la communauté **Logiciels Libres** qui utilisent les mêmes logiciels.

Dans ce but, toute aide des disrooters est toujours nécessaire et la bienvenue. Ainsi, si vous pensez que tutoriel manque, est incorrect ou pourrait être amélioré, n'hésitez pas à nous contacter et (encore mieux), écrivez le tutoriel vous-même.<br>

Nous utilisons **[Git](https://fr.wikipedia.org/wiki/Git)**, donc vous pouvez simplement demander un "pull" de notre repository. Si vous n'êtes pas familier avec cet outil, vous pouvez soumettre vos changements ou suggestion via etherpard ou email. Vous trouverez [**ici**](/contribute/git) a tuto git très simple.<br>

De plus, nous sommes en train de construire un glossaire pour aider toutes les personnes qui ont des difficultés avec certains termes et concepts. Vous pouvez le retrouvez [**ici**](/glossary).

En avant ...
