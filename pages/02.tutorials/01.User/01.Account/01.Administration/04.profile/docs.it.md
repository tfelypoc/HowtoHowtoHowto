---
title: 'Account Profile'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - profile
page-toc:
    active: 
---

# Aggiorna il tuo profilo
Che si tratti di ricevere notifiche o reimpostare la password, puoi aggiungere un indirizzo email secondario al tuo profilo. Puoi anche modificare il tuo screen name (il nome che apparirà sulle schermate dei servizi). 

![](en/dashboard_profile.png)

Completa e aggiorna le tue informazioni.

![](en/profile_update.png)

- **Screen Name**: questo è il nome che scegli per identificarti attraverso diversi servizi che richiedono credenziali **Disroot**. 
- **Email di notifica**: l'indirizzo email dove riceverai informazioni importanti relative al tuo account. Di tanto in tanto, potresti anche ricevere informazioni su di noi, come annunci di servizi o miglioramenti o il rapporto annuale **Disroot**. 
- **E-mail di reimpostazione password**: puoi aggiungere/modificare un indirizzo email secondario in modo da poterlo utilizzare per reimpostare la tua password nel caso in cui la perdi/dimentichi. 