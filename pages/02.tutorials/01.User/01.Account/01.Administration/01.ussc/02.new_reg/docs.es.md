---
title: 'Registro de una nueva cuenta'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - cuenta
        - registro
page-toc:
    active: false
---

# Registro de una nueva cuenta
Aunque el proceso es bastante simple, lo veremos paso a paso.

!! **Al momento de hacer la solicitud, ten en cuenta lo siguiente:**
!! - Utiliza el mismo dispositivo desde el que completas la solicitud para abrir el link de verificación (caso contrario, el proceso puede fallar).
!! - Pasados 15 minutos, la sesión expirará y será necesario iniciar el proceso de nuevo.
!! - Si usas TOR o una VPN y la IP cambia durante el proceso, el sistema lo interpretará como un ataque y la sesión será bloqueada.


## Registro paso a paso
###  01: El nombre de Usuarix
Este será el nombre de la cuenta y la dirección de correo y no se puede modificar una vez que sido creado. El nombre de la cuenta tiene el formato: _**nombredeusuarix@disroot.org**_

![](en/reg_01.png)


### 02: El nombre en pantalla
Es el nombre que se mostrará en los servicios que requieren inicio de sesión, como la **Nube** o el **Correo**. Habitualmente, es el mismo que el nombre de usuarix, pero puedes elegir el que quieras.<br>
Por ejemplo, tu nombre de usuarix podría ser _nombredeusuarix_ y tu nombre en pantalla _Nombre de Usuarix_.

![](en/reg_02.png)


### 03: La pregunta
El objetivo de esta pregunta es verificar que quien realiza el registro es una persona y no un bot, y también para desalentar a los spammers todo lo que se pueda.

!! **La respuesta debe cumplir con los siguientes requisitos**:
!! - Debe tener, por lo menos, 150 caracteres de longitud.
!! - Debe ser la respuesta a la pregunta, nada más.
!! - No puede ser una cita o un copy/paste de una fuente externa.
!! - Puede ser en español, aunque sugerimos hacerlo en inglés.
!! - No puede contener caracteres especiales de otro idioma que no sea inglés. Es decir, se debe evitar el uso de tildes o, por ejemplo, la letra ñ.

![](en/reg_03.png)


### 04: El correo de verificación
Para completar el proceso de registro, es necesario enviarte un código de verificación a una dirección de correo electrónico válida. También en este paso, puedes elegir usar esa misma dirección en caso que necesites restablecer la contraseña en el futuro. Si prefieres no hacerlo, **recuerda que debes mantener tu contraseña segura ya que la dirección de correo utilizada para la verificación se elimina de nuestra base de datos una vez que la solicitud se aprueba/deniega y no podemos restablecerla**.

![](en/reg_04.png)

Luego ingresa la contraseña dos veces, verificando que cumpla con las condiciones para su creación, y haz clic en el botón **Continue** para terminar.

¡Y eso es todo! **\0/**

!! #### AVISO<br>
!! **Las solicitudes de cuentas son revisadas a diario manualmente, una por una, por lo tanto la aprobación podría demorar hasta 48 horas.**
