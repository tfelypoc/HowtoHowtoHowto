---
title: 'Информация об учётной записи'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - information
page-toc:
    active: 
---

# Информация о вашей учётной записи

![](ru/dashboard_info.png)

Здесь вы найдёте сводную информацию об **учётной записи** и **управлении паролями**, а также **политике паролей** (условия, которым должен соответствовать пароль).

![](ru/account.png)

Этот раздел носит исключительно информативный характер.
