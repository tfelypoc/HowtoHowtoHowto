---
title: RGPD: Nextcloud
published: true
indexed: false
updated:
    last_modified: "Juin 2020"
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - email
        - rgpd
visible: true
page-toc:
    active: false
---
## Comment exporter vos ...

  - [**Fichiers et Notes**](files)
  - [**Contacts**](contacts)
  - [**Calendriers**](calendar)
  - [**Favoris**](bookmarks)
