---
title: 'GDPR - Exportar os seus dados pessoais por si próprio'
published: true
visible: true
taxonomy:
    category:
        - docs
visible: true
page-toc:
    active: false

---

No Disroot basicamente todos os dados que temos são aqueles que você fornece quando usa os diferentes serviços (nós armazenamos os seus ficheiros porque você escolhe armazenar os seus ficheiros na nossa cloud). Nós não temos interesse em adquirir e recolher quaisquer dados extra nem os processamos para os vender a empresas de publicidade ou para os monetizar de qualquer forma. Devido a isso a maioria dos nossos serviços fornece-lhe uma maneira para você poder exportar os seus dados por si  próprio. Esta secção contém tutoriais que o irão ajudar a descarregar todos os dados armazenados nos serviços do Disroot relacionados com a sua conta de utilizador.

## Tabela de Conteúdos
- [Nextcloud](nextcloud)
  - [Nextcloud - Files](nextcloud/Files)
  - [Nextcloud - Contacts](nextcloud/Contacts)
  - [Nextcloud - Calendars](nextcloud/Calendar)
  - [Nextcloud - Bookmarks](nextcloud/Bookmarks)
  - [Nextcloud - Notes](nextcloud/Files)
- [Forum / Discourse](discourse)
- [Board / Taiga](taiga)
