---
title: "Taiga: Project export"
published: true
visible: true
indexed: true
updated:
    last_modified: "February 2022"		
    app: Taiga
    app_version: 4.0.0
taxonomy:
    category:
        - docs
    tags:
        - user
        - taiga
        - gdpr
    visible: true
page-toc:
    active: false
---

Segui i seguenti passi per esportare i dati dei tuoi progetti dall'interfaccia di **Taiga**:

- Vai su https://board.disroot.org e accedi.
- Seleziona il progetto che desideri esportare e vai alle impostazioni dell'amministratore.
- Nella scheda **Progetto** troverai l'opzione **Esporta**. Fare clic sul pulsante **ESPORTA** per generare un file .json con tutte le informazioni del progetto. Puoi usarlo come backup o per iniziare un nuovo progetto basato su di esso.

![](en/export.gif) 
