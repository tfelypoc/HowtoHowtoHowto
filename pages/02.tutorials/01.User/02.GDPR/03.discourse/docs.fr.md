---
title: "Discourse: Exportez vos posts du Forum"
published: true
indexed: true
updated:
    last_modified: "Juin 2020"		
    app: Discourse
    app_version: 2.3
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - forum
        - discourse
        - rgpd
visible: true
page-toc:
    active: false
---

**Discourse**, le logiciel utilisé par **Disroot** pour son **Forum**, vous permet d'exporter le contenu textuel de tous vos posts vers un fichier .csv (qui est supporté par la plupart des logiciels de tableurs, Libreoffice, Openoffice, Gnumeric, Excel).

**Pour exporter vos messages depuis Discourse :**
- Connectez-vous dans le **Forum**
- Cliquez sur votre avatar d'utilisateur dans le coin supérieur droit de l'écran.
- Appuyez sur le bouton avec votre nom d'utilisateur

  ![](en/export.gif)

- Appuyez sur le bouton **Télécharger tout**.

  ![](en/download_1.png)

- Une fenêtre pop-up apparaîtra pour vous demander si vous voulez télécharger vos messages, puis appuyez sur **OK**.

  ![](en/download_2.png)

- Le système commencera à traiter vos données et vous enverra une notification lorsqu'elles seront prêtes à être téléchargées.

  [ ](en/download_3.png)

  ![](en/notification.png)

- Vous recevrez un message du système vous informant que les données sont prêtes à être téléchargées et vous fournissant un lien pour télécharger le fichier .csv avec une copie de vos messages. Si vous avez activé les notifications par e-mail, vous recevrez également un e-mail contenant ces informations.

- Cliquez sur le lien pour télécharger le fichier.

  ![](en/notification_2.png)  

- Le lien sera disponible pendant 48h, après quoi il expirera et vous devrez exporter vos données à nouveau.

- Une fois que vous avez extrait le fichier, vous pouvez l'ouvrir dans votre tableur.


**NOTE** : Les données ne peuvent être téléchargées qu'une fois par 24h
