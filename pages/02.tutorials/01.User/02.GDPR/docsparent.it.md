---
title: Esportare i propri dati personali / Conformità al GDPR
visible: true
published: true
indexed: true
updated:
    last_modified: "February 2022"
taxonomy:
    category:
        - docs
    tags:
        - user
        - personal data
        - gdpr
    visible: true
page-toc:
    active: false

---

# Come esportare i propri dati personali

In **Disroot** praticamente tutti i dati raccolti sono quelli forniti dall'utente durante l'utilizzo dei servizi (archiviamo i tuoi file perché decidi di archiviarli sul nostro cloud). Non abbiamo alcun interesse ad acquisire e raccogliere dati extra né elaborarli per venderli a società pubblicitarie o utilizzare in alcun modo per monetizzare su di essi. Pertanto, la maggior parte dei servizi offre un modo per esportare autonomamente i dati. Questo capitolo include tutorial che ti aiuteranno a ottenere tutti i dati archiviati nei servizi forniti da **Disroot** e collegati al tuo account.

## Contents
- [Cloud (Nextcloud)](nextcloud)
  - [File & Note](nextcloud/files)
  - [Contatti](nextcloud/contacts)
  - [Calendari](nextcloud/calendar)
  - [Segnalibri](nextcloud/bookmarks)
- [Forum (Discourse)](discourse)
- [Board (Taiga)](taiga)

---
