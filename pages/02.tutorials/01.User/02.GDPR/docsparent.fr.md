---
title: Exportez vos Données Personnelles / Conformité RGPD
visible: true
published: true
indexed: true
updated:
    last_modified: "December 2020"
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - données personnelles
        - rgpd
    visible: true
page-toc:
    active: false

---

#  Vos données sont les vôtres:<br>Comment exporter soi-même ses données personnelles

Chez **Disroot**, toutes les données collectées sont celles que vous fournissez en utilisant nos services (*nous stockons vos fichiers parce que vous avez décidé de stocker vos fichiers sur notre cloud*).

Nous n'avons aucun intérêt dans l'acquisition et la collecte de données supplémentaires, ni dans le traitement de celles-ci en vue de les vendre à des compagnies publicitaires, ni en aucune façon de capitaliser dessus. Dans cette optique, la plupart de nos services vous offrent la possibilité d'exporter vos données vous-même

 Ce chapitre inclut des tutoriels qui vous aideront à récupérer toutes les données stockées sur les serveurs de **Disroot** vous concernant/liées à votre compte.

----

## Exportez vos données depuis:
- [01. Email (Rainloop)](rainloop)
- [02. Cloud (Nextcloud)](nextcloud)
  - [Fichiers & Notes](nextcloud/files)
  - [Contacts](nextcloud/contacts)
  - [Calendriers](nextcloud/calendar)
  - [Favoris](nextcloud/bookmarks)
- [03. Forum (Discourse)](discourse)
- [04. Board (Taiga)](taiga)
- [05. Git (Gitea)](git)
