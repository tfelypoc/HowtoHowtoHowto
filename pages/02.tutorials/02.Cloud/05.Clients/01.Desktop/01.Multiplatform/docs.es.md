---
title: "Clientes de Escritorio: Multiplataforma"
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - sync
page-toc:
    active: false
---

# Multiplatform Clients


### [Cliente Nextcloud](desktop-sync-client)
- Cliente de escritorio para sincronización

### [Thunderbird](thunderbird-calendar-contacts)
- Calendario, Contactos & sincronización de Tareas

### [calcurse](calcurse-caldav)
- Sincronización de Calendario para la línea de comando
