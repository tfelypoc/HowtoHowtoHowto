---
title: GNU/Linux
published: true
updated:
taxonomy:
    category:
            - docs
    tags:
            - cloud
            - desktop
            - integration
            - clients
            - gnu-linux
visible: true
page-toc:
     active: false
---

# GNU/Linux Desktop clients

## [GNOME Desktop Integration](gnome-desktop-integration)

## [KDE Desktop Integration](kde-desktop-integration)

## [Terminal Integration](terminal-integration)
