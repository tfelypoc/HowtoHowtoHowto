---
title: Forms
published: true
visible: false
indexed: true
updated:
        last_modified: " 2022"
        app: Forms
        app_version: 2.1.0
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - forms
visible: true
page-toc:
    active: false
---

# Forms (coming soon)

**Moduli** fornisce semplici sondaggi e questionari. 
