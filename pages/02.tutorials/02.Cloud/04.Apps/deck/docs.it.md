---
title: Deck
published: true
visible: false
indexed: true
updated:
        last_modified: " 2022"
        app: Deck
        app_version: 1.2.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - deck
visible: true
page-toc:
    active: false
---

# Deck 

**Deck** è uno strumento di organizzazione in stile kanban finalizzato alla pianificazione personale e all'organizzazione di progetti per i team integrati con il tuo **Disroot Cloud**. 
