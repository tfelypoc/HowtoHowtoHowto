---
title: Deck
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Deck
        app_version: 1.2.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - deck
visible: true
page-toc:
    active: false
---

# Deck (bientôt disponible)

**Deck** est un outil d'organisation de style kanban destiné à la planification personnelle et à l'organisation de projets pour les équipes, intégré à votre **Disroot Cloud**.
