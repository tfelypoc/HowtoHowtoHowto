---
title: Dashboard
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - dashboard
visible: true
page-toc:
    active: false
---

# Dashboard

La **Dashboard** ti offre una panoramica dei tuoi prossimi appuntamenti, e-mail, messaggi di chat, biglietti in arrivo, ultime notizie e molto altro.

## [Integrazione mastodon](mastodon)
- Widget **Dashboard** che mostrano le notifiche importanti e la cronologia principale di **Mastodon**.

## [Integrazione con Zammad](Zammad)
- Un widget **Dashboard** che mostra le notifiche, un provider di ricerca per i ticket e le notifiche per i nuovi ticket aperti su **Zammad**. 
