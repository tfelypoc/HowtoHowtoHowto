---
title: Dashboard
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - dashboard
visible: true
page-toc:
    active: false
---

# Dashboard (coming soon)

The **Dashboard** gives you an overview of your upcoming appointments, emails, chat messages, incoming tickets, latest toots and much more.

## [Mastodon integration](mastodon)
- **Dashboard** widgets that displays important notifications and the **Mastodon** home timeline.

## [Zammad integration](zammad)
- A **Dashboard** widget that displays notifications, a search provider for tickets and notifications for new open tickets on **Zammad**.
