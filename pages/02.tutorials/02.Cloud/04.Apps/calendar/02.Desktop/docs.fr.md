---
title: Bureau
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Clients de bureau pour le calendrier

## [Thunderbird](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)
- Gérez vos calendriers, contacts et tâches

## [calcurse](/tutorials/cloud/clients/desktop/multiplatform/calcurse-caldav)
- Gérez vos calendriers à partir de la ligne de commande

---

Tutos connexes :

- [GNOME Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [Intégration du bureau KDE](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
