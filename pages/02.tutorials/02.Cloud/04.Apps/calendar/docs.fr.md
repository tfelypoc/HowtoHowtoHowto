---
title: Calendrier
published: true
visible: false
indexed: true
updated:
        last_modified: "Décembre 2020"
        app: Calendrier
        app_version: 2.1.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - appli
        - Calendrier
page-toc:
    active: false
---

# Calendrier
L'application **Calendar** est l'interface utilisateur du serveur CalDAV de **Disroot**. Elle vous permet de synchroniser les événements de divers appareils avec votre **Disroot Cloud** et de les modifier en ligne.

---

## [Interface web](web)
- Création et configuration de calendriers

## [Clients de bureau](desktop)
- Clients de bureau et paramètres d'intégration pour organiser et synchroniser les calendriers.

## [Clients mobiles](/tutorials/cloud/clients/mobile)
- Clients mobiles et paramètres d'organisation et de synchronisation des calendriers.
