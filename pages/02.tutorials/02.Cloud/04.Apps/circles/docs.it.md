---
title: Circles
published: true
visible: false
indexed: true
updated:
        last_modified: " 2022"
        app: Circles
        app_version: 0.20.6
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - circles
visible: true
page-toc:
    active: false
---

# Circles

**Circles** ti permette di creare i tuoi gruppi di utenti/colleghi/amici. Tali gruppi (o "cerchie") possono quindi essere utilizzati da qualsiasi altra app per scopi di condivisione (file, feed social, aggiornamento dello stato, messaggistica, ecc.). 
