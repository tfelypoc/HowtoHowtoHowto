---
title: Notes: application portable
published: true
visible: false
updated:
        last_modified: "Juillet 2019"
        app:
        app_version:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - notes
        - android
        - portable
page-toc:
    active: false
---

## Android
#### [Appli Notes](android)

----
### Tutos associés
#### [Appi portable Nexcloud](/tutorials/cloud/clients/mobile/android/nextcloud-app)
