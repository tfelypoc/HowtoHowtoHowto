---
title: Notes
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - notes
visible: true
page-toc:
    active: false
---

# Notes

**Notes** is a distraction free notes taking app. It provides categories for better organization and supports formatting using **Markdown** syntax. Notes are saved as files in your **Disroot Cloud**, so you can view and edit them with every Nextcloud client.

---

## [Web interface](web)
- Creating and editing Notes

## [Desktop clients](desktop)
- Desktop clients and taking notes apps to create and sync notes

## [Mobile clients](mobile/android)
- Mobile clients and taking notes apps to create and sync notes
