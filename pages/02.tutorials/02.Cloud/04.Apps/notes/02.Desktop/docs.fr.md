---
title: Bureau
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - notes
        - bureau
page-toc:
    active: false
---
# Clients du bureau Notes

Pour utiliser **Notes** (les créer, les modifier et les partager), veuillez consulter les tutoriels suivants :


## [Application Nextcloud](/tutoriels/cloud/clients/desktop/multiplatform/desktop-sync-client)

#### [Synchronisation des tâches avec Thunderbird](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts#tasks-integration-with-with-thunderbird)

----
## Tutos connexes

- [GNOME : Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [KDE : Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
