---
title: "Apps"
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Applis Cloud

L'une des caractéristiques les plus puissantes de **Nextcloud** est la possibilité d'étendre ses fonctionnalités grâce à des **applications**. Il s'agit essentiellement de plugins ou d'extensions qui permettent d'ajouter des fonctions supplémentaires à l'environnement **Cloud**.

Vous trouverez ci-dessous des guides et des tutoriels sur celles qui sont actuellement disponibles dans le **Disroot Cloud** pour apprendre à les utiliser ou à les configurer sur différents appareils.
