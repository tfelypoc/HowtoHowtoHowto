---
title: Tâches
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - task
        - sync
    visible: true
page-toc:
    active: false
---

# Tâches
**Tâches** vous permet d'ajouter, de modifier et de supprimer des tâches dans votre **Cloud**. Elles peuvent être partagées entre utilisateurs, synchronisées à l'aide de [**CalDav**](https://en.wikipedia.org/wiki/CalDAV) pour les synchroniser avec votre client local et même téléchargées en tant que fichiers [ICS](https://en.wikipedia.org/wiki/ICalendar).

---

## [Interface Web](web)
- Création et configuration de tâches

## [Clients de bureau](desktop)
- Clients de bureau et applications pour organiser et synchroniser les tâches.

## [Clients mobiles](mobile)
- Clients mobiles et paramètres pour l'organisation et la synchronisation des tâches.
