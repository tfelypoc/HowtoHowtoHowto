---
title: Mobile
published: true
visible: false
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - mobile
page-toc:
    active: false
---

# Tasks mobile integration

To set up and sync your **Tasks** throught a mobile client see the tutorial below:

## Android
- [DAVx⁵ / OpenTasks](/tutorials/cloud/clients/mobile/android/calendars-contacts-and-tasks)
- [Nextcloud mobile app](/tutorials/cloud/clients/mobile/android/nextcloud-app)
