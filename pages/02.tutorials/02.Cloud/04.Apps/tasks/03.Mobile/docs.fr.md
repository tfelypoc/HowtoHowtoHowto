---
title: Portable
published: true
visible: false
updated:
        last_modified: "Juillet 2019"
taxonomy:
    category:
        - docs
    tags:
        - tâche
        - cloud
        - portable
page-toc:
    active: false
---

# Intégration mobile des Tâches

Pour configurer et synchroniser vos **Tâches** via un client mobile, consultez le tutoriel ci-dessous :

## Android
- [DAVx⁵ / OpenTasks](/tutorials/cloud/clients/mobile/android/calendriers-contacts-et-tâches)
- [Application mobile Nextcloud](/tutorials/cloud/clients/mobile/android/nextcloud-app)
