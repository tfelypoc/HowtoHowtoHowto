---
title: Portables
published: true
visible: false
updated:
        last_modified: "Juillet 2019"
        app:
        app_version:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - contacts
page-toc:
    active: false
---

# Intégration de l'application Contacts

L'application Contacts de **Disroot** est activée.

Pour configurer et utiliser les contacts **Disroot** sur votre appareil, consultez le tutoriel suivant :

### [Nexctcloud : Clients mobiles](tutorials/cloud/clients/mobile)
