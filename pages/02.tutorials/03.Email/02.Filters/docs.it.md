---
title: "Filters: Configuring"
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - filters
        - settings
page-toc:
    active: false
---

# Filtri email

I filtri e-mail consentono di gestire le e-mail in arrivo in modo automatizzato, ad esempio spostandole in una directory in base a determinati criteri, impostando la risposta automatica fuori sede/vacanze, rifiutando o inoltrando automaticamente le e-mail.

In questa sezione tratteremo le nozioni di base sulla base di alcuni scenari.


### [Inoltra i tuoi e-mail](forward)
