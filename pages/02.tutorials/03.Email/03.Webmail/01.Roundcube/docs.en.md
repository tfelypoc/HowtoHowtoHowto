---
title: "Roundcube Webmail Client"
published: true
visible: true
indexed: true
updated:
        last_modified: "April, 2021"
        app: Roundcube Webmail
        app_version: 1.4.4
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: true
---

![](rc_logo.png)

# Table of Contents

## [01. Getting started](getting_started)
  - Overview
  - Tasks and basic operations

## [02. Settings](settings)
  - [01. Preferences](settings/preferences)
    - User interface
    - Mailbox view
    - Displaying messages
    - Composing messages
    - Contacts
    - Special folders
    - Server settings
    - Deleting old messages
    - Message highlights
  - [02. Folders](settings/folders)
  - [03. Identities](settings/identities)
    - Default identity
    - Add other identities / aliases
    - Sending an email with another identity
  - [04. Responses](settings/responses)
  - [05. Filters](settings/filters)
  - [06. Account details](settings/account_details)

## [03. Email](email)
  - Composing an email

## [04. Contacts](contacts)
  - Contacts lists
  - Address book
  - Groups
