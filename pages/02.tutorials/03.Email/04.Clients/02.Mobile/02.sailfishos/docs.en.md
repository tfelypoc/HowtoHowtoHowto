---
title: Mobile clients: SailfishOS mail client
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Email on SailfishOS

Setting up your **Disroot** mail on SailfishOS is very easy. Just follow those simple steps. (it took longer to make those fancy screenshots :P )


1. Open up **Settings** app

2. Go all the way to the bottom (Sailfish2.0) to **Accounts** tab

3. Select **General email**

![](en/sailfish_mail1.png)

4. Fill in your **disroot** email address and password and swipe "Accept".

![](en/sailfish_mail2.png)

5. Server settings.

 - **Incoming mail server:**
    - Edit username and **remove the domain leaving only the username**
    - Add server address: **disroot.org**
    - **Enable SSL connection**

![](en/sailfish_mail3.png)

 - **Outgoing mail server:**
    - Server address: **disroot.org**
    - Secure connection: **StartTLS**
    - Port: **587**
    - **Authentication required**

![](en/sailfish_mail4.png)

6. Swipe "**Accept**"

7. Edit details such as **description** and "**Your name**", and swipe "**Accept**"

![](en/sailfish_mail5.png)

**You're done!** \o/
