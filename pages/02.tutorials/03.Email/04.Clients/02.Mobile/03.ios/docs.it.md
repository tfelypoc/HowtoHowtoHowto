---
title: Mobile client: iOS mail client
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Email su iOS

1. Apri le impostazioni del tuo dispositivo iOS e vai su "Posta, contatti, calendari". Quindi seleziona "Aggiungi account".

![](en/ios_mail1.PNG)

2. Seleziona 'Altri'.

![](en/ios_mail2.PNG)

3. Seleziona 'Aggiungi un Account email'.

![](en/ios_mail3.PNG)

4. Inserisci le tue credenziali e clicca su 'Avanti'.

![](en/ios_mail4.PNG)

5. Modificare il nome host in disroot.org, sia per il server di posta in entrata che in uscita.

![](en/ios_mail5.PNG)

Fai clic su "Avanti" e il tuo account dovrebbe essere pronto per l'uso all'interno del tuo client di posta elettronica iOS.
