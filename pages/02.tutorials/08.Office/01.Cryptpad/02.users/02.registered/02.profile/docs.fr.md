---
title: 'Profil'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - profil
        - paramètres
        - cryptpad
page-toc:
    active: true
---

# Profil
Ici, vous pouvez personnaliser :

![](en/user_profile.png)

1. L'**avatar**, en téléchargeant une image.

2. Ajoutez une description.

![](en/description.png)

3. Ajoutez un lien vers un site web.

![](en/add_website.png)

4. Partagez le profil via un lien.

![](en/share_profile.png)
