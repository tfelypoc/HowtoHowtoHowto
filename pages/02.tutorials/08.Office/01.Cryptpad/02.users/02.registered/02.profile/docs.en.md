---
title: 'Profile'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - profile
        - settings
        - cryptpad
page-toc:
    active: true
---

# Profile
Here you can customize:

![](en/user_profile.png)

1. The **avatar**, by uploading an image.

2. Add a description.

![](en/description.png)

3. Add a link to a website.

![](en/add_website.png)

4. Share the profile via a link.

![](en/share_profile.png)
