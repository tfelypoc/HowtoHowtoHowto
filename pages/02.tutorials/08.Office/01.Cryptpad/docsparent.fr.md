---
title: 'CryptPad'
updated:
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - office
        - collaboration
page-toc:
  active: false
---

![](en/cryptpad_logo.svg)

# Qu'est-ce que CryptPad ?

**CryptPad** est une puissante suite d'outils de collaboration et de bureautique en ligne qui vous permet de créer, de partager et de travailler avec d'autres (et en temps réel) sur des documents texte, des feuilles de calcul, des présentations, des sondages, des tableaux blancs, de l'édition de code et des [tableaux Kanban](https://en.wikipedia.org/wiki/Kanban_board) pour des projets, dans un environnement sécurisé et privé. Cela signifie que le contenu de tous vos travaux et fichiers est crypté et décrypté dans votre navigateur web ; personne (pas même les administrateurs du serveur) ne peut donc accéder à vos informations.

À bien des égards, il est similaire aux solutions propriétaires bien connues et peu respectueuses de la vie privée, telles que **Google Docs**. La principale différence est que **CryptPad** est un [**logiciel libre/libre**] (https://en.wikipedia.org/wiki/Free_software) et qu'il est conçu pour être **privé**, avec **zéro connaissance** du serveur où il fonctionne.

Dans les chapitres suivants, vous trouverez les informations nécessaires pour commencer à utiliser et à vous familiariser avec **CryptPad**.


# [01. Comptes](accounts)
- Types de comptes
- Enregistrement d'un nouveau compte

# [02. Utilisateurs](users)
Configurations et fonctionnalités pour

- [**Utilisateur invité**](users/guest)

- [**Utilisateurs enregistrés**](users/registered)
