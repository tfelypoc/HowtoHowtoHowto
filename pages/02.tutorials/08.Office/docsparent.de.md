---
title: Office
subtitle: "Pads, Pastebin, Mumble, Polls & File Sharing"
icon: fa-file-text
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - office
        - cryptpad
        - pastebin
        - file
        - sharing
        - disapp
        - Austausch
        - Zusammenarbeit
page-toc:
    active: false
---

# Office-Tools

Disroot stellt einige nützliche Online-Werkzeuge zur Verfügung, die Du vielleicht mal ausprobieren möchtest.

---

Für ein besonders komfortables Nutzererlebnis gibt es die **Disroot**-App, die Dir schnellen Zugriff auf diese Werkzeuge und noch viel mehr **Disroot**-Angebote bietet:

### [DisApp](../user/disapp)
