---
title: Glossaire
published: false
visible:
taxonomy:
    category:
        - docs
    tags:
        - glossary
page-toc:
    active: false
---

# Glossaire (Arrive bientôt)

- Git
- Pull request
- Server
- Instance
- Cloud
- GPDR
