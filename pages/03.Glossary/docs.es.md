---
title: Glosario
published: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

- Git
- Pull request
- Server
- Instance
- Cloud
- GPDR
